/* test1.c - test that transaction fails if segment already in another transaction */

#include "rvm.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SEGNAME0  "testseg1"
#define SEGNAME1  "testseg2"
#define SEGNAME2  "testseg3"

#define OFFSET0  10

#define GOOD_STRING "hello, world"


int main(int argc, char **argv)
{
     rvm_t rvm;
     char* segs[3];
     trans_t trans1, trans2;

     /* initialize */
     rvm = rvm_init("rvm_segments");

     rvm_destroy(rvm, SEGNAME0);
     rvm_destroy(rvm, SEGNAME1);
     rvm_destroy(rvm, SEGNAME2);

     segs[0] = (char*) rvm_map(rvm, SEGNAME0, 1000);
     segs[1] = (char*) rvm_map(rvm, SEGNAME1, 1000);
     segs[2] = (char*) rvm_map(rvm, SEGNAME2, 500);


     /* write in some initial data */
     trans1 = rvm_begin_trans(rvm, 2, (void **) segs);

     trans2 = rvm_begin_trans(rvm, 2, (void **) (segs+1));
     if (trans2 != -1){
	printf("ERROR second transaction should have failed\n");
	exit(2);
     }

     printf("OK\n");
     return 0;
}
