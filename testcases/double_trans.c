/* double_trans.c - test that two transactions cannot be started on the same segment*/

#include "rvm.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>


int main(int argc, char **argv) 
{
	rvm_t rvm;
	char* seg;
	trans_t trans1;
	int test;

	printf("The test is OK if it exits smoothly (ie the double transaction error is handled)\n");
	rvm = rvm_init("rvm_segments");
	rvm_destroy(rvm, "testseg");
	seg = (char *) rvm_map(rvm, "testseg", 128);
	trans1 = rvm_begin_trans(rvm, 1,(void**)&seg);
	trans1 = rvm_begin_trans(rvm, 1,(void**)&seg);
	rvm_unmap(rvm, seg);
	test = (int)trans1;
	printf("cast trans into an integer is ok if %d = %d\n",test,trans1);


}
