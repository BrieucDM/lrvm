/* unmap_log.c - test that logs go away when the segment is unmapped*/

#include "rvm.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>

#define TEST_STRING1 "hello, world"
#define TEST_STRING2 "bleg!"

int main(int argc, char **argv) 
{
	rvm_t rvm;
	char* seg;
	trans_t trans1;

	printf("The test is OK if logs are displayed and then diseappear\n");
	rvm = rvm_init("rvm_segments");
	rvm_destroy(rvm, "testseg");
	seg = (char *) rvm_map(rvm, "testseg", 128);
	trans1 = rvm_begin_trans(rvm, 1,(void**)&seg);
	rvm_about_to_modify(trans1, seg, 0, 100);
     	sprintf(seg, TEST_STRING1);
	rvm_commit_trans(trans1);
	printf("***before unmap***\n");
	system("ls -l rvm_segments/testseg.*");
	rvm_unmap(rvm, seg);
	printf("***after unmap***\n");
	system("ls -l rvm_segments/testseg.*");

}
