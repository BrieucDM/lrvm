/* test2.c - test if it possible to map twice the same segment */

#include "rvm.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SEGNAME0  "testseg1"


int main(int argc, char **argv)
{
     rvm_t rvm;
     char* segs[2];
     
     /* initialize */
     rvm = rvm_init("rvm_segments");

     rvm_destroy(rvm, SEGNAME0);

     segs[0] = (char*) rvm_map(rvm, SEGNAME0, 1000);
     segs[1] = (char*) rvm_map(rvm, SEGNAME0, 2000);

     if (segs[1] != NULL){
	 printf("ERROR rvm_map shouldn't allow to map twice the same segment\n");
	  exit(2);
     }

     printf("OK\n");
     return 0;
}
