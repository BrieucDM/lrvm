/* test3.c - truncate while in transaction */

#include "rvm.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>

#define SEGNAME0  "testseg1"
#define SEGNAME1  "testseg2"

#define OFFSET0  10
#define STRING0 "hello, world"

void proc1() 
{
     rvm_t rvm;
     char* segs[2];
     trans_t tx1, tx2;
     
     /* initialize */
     rvm = rvm_init("rvm_segments");

     rvm_destroy(rvm, SEGNAME0);
     rvm_destroy(rvm, SEGNAME1);

     segs[0] = (char*) rvm_map(rvm, SEGNAME0, 1000);
     segs[1] = (char*) rvm_map(rvm, SEGNAME1, 5000);
	
     /* write in some initial data */
     tx1 = rvm_begin_trans(rvm, 1, (void **) segs);
     rvm_about_to_modify(tx1, segs[0], OFFSET0, 100);
     strcpy(segs[0]+OFFSET0, STRING0);
     rvm_commit_trans(tx1);
     rvm_truncate_log(rvm);
 
     tx2 = rvm_begin_trans(rvm, 1, (void **) (segs+1));
     rvm_about_to_modify(tx2, segs[1], OFFSET0, 100);
     strcpy(segs[1]+OFFSET0, STRING0);

     //rvm_commit_trans(tx1);

     abort();
}


void proc2() 
{
     rvm_t rvm;
     char *segs[2];

     rvm = rvm_init("rvm_segments");
     segs[0] = (char*) rvm_map(rvm, SEGNAME0, 1000);
     segs[1] = (char*) rvm_map(rvm, SEGNAME1, 1000);

     if(strcmp(segs[0] + OFFSET0, STRING0)) {
	  printf("ERROR in segment 0 (%s)\n",
		 segs[0]+OFFSET0);
	  exit(2);
     }
     if(!strcmp(segs[1] + OFFSET0, STRING0)) {
	  printf("ERROR in segment 1 (%s)\n",
		 segs[1]+OFFSET0);
	  exit(2);
     }

     printf("OK\n");
}


int main(int argc, char **argv) 
{
     int pid;

     pid = fork();
     if(pid < 0) {
	  perror("fork");
	  exit(2);
     }
     if(pid == 0) {
	  proc1();
	  exit(0);
     }

     waitpid(pid, NULL, 0);

     proc2();

     return 0;
}
