/* double_mapping.c - test that a segment  cannot be mapped two times*/

#include "rvm.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>


int main(int argc, char **argv) 
{
	rvm_t rvm;
	char* seg;

	printf("The test is OK if it exits smoothly (ie the double mapping erro is handled)\n");
	rvm = rvm_init("rvm_segments");
	rvm_destroy(rvm, "testseg");
	seg = (char *) rvm_map(rvm, "testseg", 128);
	seg = (char *) rvm_map(rvm, "testseg", 128);
	rvm_unmap(rvm, seg);
	

}
