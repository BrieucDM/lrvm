/* destroy_mapping.c - test that a segment  cannot be destroyed when mapped*/

#include "rvm.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>


int main(int argc, char **argv) 
{
	rvm_t rvm;
	char* seg;

	printf("The test is OK if it exits smoothly (ie the destroy error is handled)\n");
	rvm = rvm_init("rvm_segments");
	rvm_destroy(rvm, "testseg");
	seg = (char *) rvm_map(rvm, "testseg", 128);
	rvm_destroy(rvm, "testseg");
	rvm_unmap(rvm, seg);
	

}
