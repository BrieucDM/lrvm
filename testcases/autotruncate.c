/* autotruncate.c - test that logs are truncated when they become too big*/

#include "rvm.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>

#define TEST_STRING1 "DEADBEEF"
#define TEST_STRING2 "ADBEEFDE"

int main(int argc, char **argv) 
{
	rvm_t rvm;
	char* seg;
	trans_t trans1;
	int i;

	printf("The test is OK if logs doesn't expand to a too high value but is regularly truncated\n");
	rvm = rvm_init("rvm_segments");
	rvm_destroy(rvm, "testseg");
	seg = (char *) rvm_map(rvm, "testseg", 128);
	for(i=1;i<65000;i++)
	{
		trans1 = rvm_begin_trans(rvm, 1,(void**)&seg);
		rvm_about_to_modify(trans1, seg, 0, 100);
	     	sprintf(seg, TEST_STRING1);
		rvm_commit_trans(trans1);
		trans1 = rvm_begin_trans(rvm, 1,(void**)&seg);
		rvm_about_to_modify(trans1, seg, 0, 100);
	     	sprintf(seg, TEST_STRING2);
		rvm_commit_trans(trans1);

	}
	system("ls -l rvm_segments/testseg.*");
	rvm_unmap(rvm, seg);


}
