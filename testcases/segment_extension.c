/* segment_extension.c - test that segment creation and extension works */

#include "rvm.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>


int main(int argc, char **argv) 
{
	rvm_t rvm;
	char* seg;
	     
	rvm = rvm_init("rvm_segments");
	rvm_destroy(rvm, "testseg");
	system("ls -l rvm_segments");
	seg = (char *) rvm_map(rvm, "testseg", 128);
	rvm_unmap(rvm, seg);
     	system("ls -l rvm_segments");
	seg = (char *) rvm_map(rvm, "testseg", 256);
	rvm_unmap(rvm, seg);
     	system("ls -l rvm_segments");
	printf("The test is OK if you see 0 file, then a file of size 128 and then a file of size 256\n");

}
