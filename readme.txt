CS6210 Homework 4 Recoverable Virtual Memory
Brieuc du Maugouer
brieuc.dumaugouer@gmail.com


The purpose of this project is to create a library implementing recoverable memory segments. The behaviour 
of this library is described in the following paper https://www.cs.berkeley.edu/~brewer/cs262/lrvm.pdf 


***How and when logs files are created during a transaction *** 

	The first log file is created when the function rvm_about_to_modify() is called. The library 
create an in memory a copy of the memory range to be modified. This log is often called the undo log.

	Later, once the program has finished modifying the memory range, the rvm_commit_trans() function 
is called. The library then compare the actual memory range with the undo log and saves the modifications 
in a log file in disk.

	From that point three scenarios are possible:

-The program crashes. The modifications will be applied to the segment at next startup using the log files.The 
log files are then deleted.

-The program calls rvm_truncate_log(). All modifications saved in the logs are applied to the corresponding 
segments and the log file is deleted

-The program calls rvm_unmap(). When the segment is unmapped, the modifications present in it's log file are 
applied and the log file is deleted. If a log file is present when the program is not running it indicates that 
it had previously crashed.

 
***What goes into log files, how are they cleaned, what prevent them to expand indefinitely*** 

-To save disk space, the log files aren't a copy of the memory segment. When rvm_commit_trans() is called, the 
old version and new version of the memory range are xor'ed. Thus we get a 1 when a bit has been modified and a 
0 when not. The log file contains the adresses of these modifications. The actual log file works as described 
above but at the byte level. Byte modifications and their adresses are stored in the log file.

-If a segment is unmapped, the modifications present in the log files are applied and the logs files destroyed. If 
the program crash, the modifications in the log will be applied at next startup and the files will be destroyed. This 
systematic removal of log files ensure that the library isn't creating files that are never deleted or keep growing 
at each execution.

-Log file flushing can be controlled by the user of the library by calling **rvm_truncate_log()**. Calling this 
function on a regular basis will make sure that the log files doesn't grow too much. The library also has a built 
in filesize control. When the function rvm_commit_trans() is called, the size of the log file is checked. If it 
is more than MAX_LOG_SIZE value, the log file is truncated.