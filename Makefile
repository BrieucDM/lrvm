#### RVM Library Makefile

CFLAGS  = -Wall -g -I. -I.lib/
LFLAGS  =
CC      = g++
RM      = /bin/rm -rf
AR      = ar rc
RANLIB  = ranlib

LIBRARY = librvm.a

LIB_SRC = lib/rvm.cpp

LIB_OBJ = $(patsubst %.cpp,%.o,$(LIB_SRC))

ABORT 	= testcases/abort.c

BASIC 	= testcases/basic.c

MULTI 	= testcases/multi.c

MULTIAB = testcases/multi-abort.c

TRUNC 	= testcases/truncate.c

EXT 	= testcases/segment_extension.c

DOUBLE 	= testcases/double_mapping.c

DESMAP 	= testcases/destroy_mapping.c

DOUBLET = testcases/double_trans.c

UMAPLOG = testcases/unmap_log.c

AUTOT 	= testcases/autotruncate.c

T1	= testcases/test1.c

T2	= testcases/test2.c

T3	= testcases/test3.c

all : $(LIBRARY) truncate multiabort multi basic abort segment_extension double_mapping destroy_mapping double_trans unmap_log autotruncate test1 test2 test3

autotruncate :
	$(CC) $(CFLAGS) $(AUTOT) -o bin/$@ $(LIBRARY)

unmap_log :
	$(CC) $(CFLAGS) $(UMAPLOG) -o bin/$@ $(LIBRARY)

double_trans :
	$(CC) $(CFLAGS) $(DOUBLET) -o bin/$@ $(LIBRARY)

destroy_mapping :
	$(CC) $(CFLAGS) $(DESMAP) -o bin/$@ $(LIBRARY)

double_mapping :
	$(CC) $(CFLAGS) $(DOUBLE) -o bin/$@ $(LIBRARY)

segment_extension :
	$(CC) $(CFLAGS) $(EXT) -o bin/$@ $(LIBRARY)

truncate : 
	$(CC) $(CFLAGS) $(TRUNC) -o bin/$@ $(LIBRARY)

multiabort : 
	$(CC) $(CFLAGS) $(MULTIAB) -o bin/$@ $(LIBRARY)

multi : 
	$(CC) $(CFLAGS) $(MULTI) -o bin/$@ $(LIBRARY)

basic : 
	$(CC) $(CFLAGS) $(BASIC) -o bin/$@ $(LIBRARY)

abort : 
	$(CC) $(CFLAGS) $(ABORT) -o bin/$@ $(LIBRARY)

test1 :
	$(CC) $(CFLAGS) $(T1) -o bin/$@ $(LIBRARY)

test2 :
	$(CC) $(CFLAGS) $(T2) -o bin/$@ $(LIBRARY)

test3 :
	$(CC) $(CFLAGS) $(T3) -o bin/$@ $(LIBRARY)



%.o: %.cpp
	$(CC) -c $(CFLAGS) $< -o $@

$(LIBRARY): $(LIB_OBJ)
	$(AR) $(LIBRARY) $(LIB_OBJ)
	$(RANLIB) $(LIBRARY)

clean:
	$(RM) $(LIBRARY) $(LIB_OBJ) bin/*
