#ifndef __LIBRVM__
#define __LIBRVM__

#include "rvm_internal.h"

/*initialize the library with the specified backing store*/
rvm_t rvm_init(const char *directory);

/*map a segment from disk into memory*/
void *rvm_map(rvm_t rvm, const char *segname, int size_to_create);

/*unmap a segment previously mapped*/
void rvm_unmap(rvm_t rvm, void *segbase);

/*destroy the backing store of a segment that is not currently mapped*/
void rvm_destroy(rvm_t rvm, const char *segname);

/*begin the transaction*/
trans_t rvm_begin_trans(rvm_t rvm, int numsegs, void **segbases);

/*declare the intent to modify a certain area of memory, can be called multiple times*/
void rvm_about_to_modify(trans_t tid, void *segbase, int offset, int size);

/*commit all changes into a log file so that if the system crash, the modifications can be applied*/
void rvm_commit_trans(trans_t tid);

/*abort the transaction, undo all changes occured during the transaction*/
void rvm_abort_trans(trans_t tid);

/*shrinks the log by applying the modifications*/
void rvm_truncate_log(rvm_t rvm);

#endif
