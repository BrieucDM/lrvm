#include "rvm.h"
seglist_t segments_head;
trans_tx  transactions_head;
int       serial;

/*initialize the library with the specified backing store----------------------------------------------------------------------------------------*/
rvm_t rvm_init(const char *directory)
{
	char* path = const_cast<char*>(directory);

	/*allocate space to store the directory*/
	rvm_t ptr = (char*)malloc(strlen(directory));
	strcpy(ptr,path);

	/*create the path if it doesn't exist*/
	if( mkdir(directory, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) == -1)
	{
		if (errno != EEXIST )
		{
			printf("error %d when creating the backing folder \n",errno);
			exit(EXIT_FAILURE);
		}
	}

	/*initialize the segments and transactions linked list*/
	segments_head = NULL;
	transactions_head =NULL;
	serial = 0;
	rvm_truncate_log(ptr);
	return ptr;
}

/*map a segment from disk into memory------------------------------------------------------------------------------------------------------------*/
void *rvm_map(rvm_t rvm, const char *segname, int size_to_create)
{
	void * segment_t;
	char * dir=(char*)malloc(strlen(segname)+strlen(rvm)+6);
	char array[4096] = {0};
	int size = 0,res;
	char* backfile = const_cast<char*>(segname);
	
	errno = 0;
	strcpy(dir,rvm);
	strcat(dir,"/");
	strcat(dir , segname);
	strcat(dir , ".seg");
	seglist_t t_segment = segments_head;
	seglist_t c_segment = segments_head;

	/*make sure that the segment hasn't been already mapped*/
	while( t_segment != NULL)
	{
		if( strcmp(t_segment->name, backfile) == 0)
		{
			printf("error segment already mapped\n");
			exit(EXIT_FAILURE);
		}
		t_segment = t_segment->next;
	}

	/*create or open the backup file*/
	int fd = open(dir, O_RDWR | O_CREAT ,  S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	if(fd <0)
	{
		printf("error while creating/opening the mapping file errno %d\n",errno);
		exit(EXIT_FAILURE);
	}

	/*extend the file if necessary*/
	size = lseek(fd, 0L, SEEK_END);
	if(size < size_to_create)
	{
		size = size_to_create - size;
		while(size !=0)
		{
			if (size > 4096)
				size = size - write(fd,array,4096);
			else	
				size = size - write(fd,array,size);
		}
	}

	/*file has been created/opened and has the right size*/
	if (fd<0)
	{
		printf("error when creating mapping file errno %d\n",errno);
		exit(EXIT_FAILURE);
	}
	c_segment = (seglist_t)malloc(sizeof(seglist));
	c_segment->size = size_to_create;
	c_segment->next = segments_head;
	c_segment->name = (char*)malloc(strlen(segname));
	c_segment->busy = FALSE;
	strcpy(c_segment->name,backfile);
	segments_head = c_segment;
	segment_t = malloc(size_to_create);	
	c_segment->addr = segment_t;

	/*copy the contents of the file to the memory*/
	lseek(fd, 0L, SEEK_SET);
	res = read(fd,c_segment->addr,size_to_create);
	if (res != size_to_create)
	{
		printf("error when copying the file contents into memory res %d errno %d\n",res,errno);
		exit(EXIT_FAILURE);
	}

	/*close the file*/
	close(fd);
	free(dir);
	return segment_t;
}

/*unmap a segment previously mapped--------------------------------------------------------------------------------------------------------------*/
void rvm_unmap(rvm_t rvm, void *segbase)
{
	seglist_t c_segment = segments_head;
	seglist_t p_segment = segments_head;
	char * filename;
	char buf,mod;
	unsigned int offset;
	int log_fd,seg_fd,size,res;

	errno = 0;

	/*check segment adress exist*/
	while(c_segment->addr != segbase)
	{
		if( c_segment == NULL)
		{
			printf("error wrong segment adress\n");
			exit(EXIT_FAILURE);
		}
		p_segment = c_segment;
		c_segment = p_segment->next;
	}
		
	
	/*open the log file*/
	filename = (char*)malloc(strlen(rvm) + strlen(c_segment->name)+6);
	strcpy(filename,rvm);
	strcat(filename,"/");
	strcat(filename,c_segment->name);
	strcat(filename,".log");
	log_fd = open(filename, O_RDWR ,  S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	if((log_fd <0) && (errno !=2))
	{
		printf("error while creating/opening the segment log file errno %d\n",errno);
		exit(EXIT_FAILURE);
	}
	else if(log_fd>0)
	{
		/*if a log file exists truncate it*/
		lseek(log_fd, 0L, SEEK_END);

		/*opening the corresponding segment*/
		size = strlen(filename);
		*(filename+size-2) = 'e';
		*(filename+size-3) = 's';
		seg_fd = open(filename, O_RDWR  ,  S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
		if(seg_fd <0)
		{
			printf("error while opening the segment file %s errno %d\n",filename,errno);
			exit(EXIT_FAILURE);
		}

		/*truncating the log file*/
		size = lseek(log_fd, 0L, SEEK_SET);
		read(log_fd,&offset,sizeof(int));
		if(offset!=0xFFFFFFFF)
		{
			printf("error wrong log file format\n");
			exit(EXIT_FAILURE);
		}

		/*skip the transcation serial number*/ 
		lseek(log_fd,sizeof(int),SEEK_CUR);
		buf = 0;
		res = 1;
		while(res !=0)
		{
			res = read(log_fd,&offset,sizeof(int));
			while(offset == 0xFFFFFFFF)
			{
				res = 0;
				offset = 0;

				/*skip the transcation serial number*/ 
				lseek(log_fd,sizeof(int),SEEK_CUR);
				res = read(log_fd,&offset,sizeof(int));
			}
			res = res + read(log_fd,&buf,1);
			if(res == 5)
			{
				/*write the modifications to the segment*/
				lseek(seg_fd,offset,SEEK_SET);
				read(seg_fd,&mod,1);
				lseek(seg_fd,offset,SEEK_SET);
				mod = buf^mod;
				write(seg_fd,&mod,1);
			}
		}
		close(log_fd);
		close(seg_fd);
		size = strlen(filename);
		*(filename+size-2) = 'o';
		*(filename+size-3) = 'l';
		if( (remove(filename) == -1))
		{
			printf("error cannot remove segment log file %s %d\n",filename,errno);
			exit(EXIT_FAILURE);
		}
	}
	free(c_segment->addr);
	free(filename);

	/*remove the segment node from the chain*/
	if((c_segment->next == NULL) && (c_segment == segments_head))
		segments_head = NULL;
	else
		p_segment->next = c_segment->next;
	
	free(c_segment->name);
	free(c_segment);
}

/*destroy the backing store of a segment that is not currently mapped----------------------------------------------------------------------------*/
void rvm_destroy(rvm_t rvm, const char *segname)
{
	seglist_t c_segment = segments_head;
	char* backfile = const_cast<char*>(segname);
	char * dir=(char*)malloc(strlen(segname)+strlen(rvm)+6);
	int size;

	errno =0;
	strcpy(dir,rvm);
	strcat(dir,"/");
	strcat(dir , segname);
	strcat(dir , ".seg");

	/*make sure that the segment isn't currently mapped*/
	while( c_segment != NULL)
	{
		if( strcmp(c_segment->name, backfile) == 0)
		{
			printf("error cannot destroy mapped segment\n");
			exit(EXIT_FAILURE);
		}
		c_segment = c_segment->next;
	}

	/*remove backing file*/
	if( (remove(dir) == -1))
	{
		if(errno !=2)
		{
			printf("error when removing segment backing file (errno %d)\n",errno);
			exit(EXIT_FAILURE);
		}
	}

	/*remove the logs*/
	size = strlen(dir);
	*(dir+size-2) = 'e';
	*(dir+size-3) = 's';
	if( (remove(dir) == -1) && (errno != ENOENT))
	{
		printf("error cannot remove segment backing file errno %d\n",errno);
		exit(EXIT_FAILURE);
	}

	free(dir);
}

/*begin the transaction--------------------------------------------------------------------------------------------------------------------------*/
trans_t rvm_begin_trans(rvm_t rvm, int numsegs, void **segbases)
{
	int i;
	seglist_t c_segment = segments_head;
	trans_tx c_trans = (trans_tx)malloc(sizeof(transact));
		
	serial = serial +1;

	/*check that segments are mapped and not involved in another transaction*/
	c_trans->segbases = (seglist_t*)malloc(numsegs*sizeof(seglist));
	c_trans->numsegs = numsegs;
	for(i = 0;i < numsegs;i++)
	{
		c_segment = segments_head;
		while(c_segment->addr != *(segbases+i))
		{
			c_segment = c_segment->next;
			if( c_segment == NULL)
			{
				printf("error segment is not mapped\n");
				exit(EXIT_FAILURE);
			}
		}
		if(c_segment->busy == TRUE)
		{
			printf("error segment is already used in another transcation\n");	
			free(c_trans->segbases);
			free(c_trans);
			printf("return\n");
			return -1;
		}
		c_segment->busy = TRUE;
		*(c_trans->segbases + i) = c_segment;
	}

	/*initialize the transaction structure*/
	c_trans->next = transactions_head;
	transactions_head = c_trans;
	c_trans->head = NULL;
	c_trans->folder = rvm;
	c_trans->serial_number = serial;
	return c_trans->serial_number;
}

/*declare the intent to modify a certain area of memory, can be called multiple times------------------------------------------------------------*/
void rvm_about_to_modify(trans_t tid, void *segbase, int offset, int size)
{
	char* ptr = (char*)segbase + offset;
	seglist_t c_segment = segments_head;
	range_t c_range = (range_t)malloc(sizeof(range));
	trans_tx c_trans = transactions_head;

	/*find the transaction in database*/
	while(c_trans->serial_number != tid)
	{
		c_trans=c_trans->next;
		if(c_trans == NULL)
		{
			printf("error transaction could not be found\n");
			exit(EXIT_FAILURE);
		}	
	}
		
	while(c_segment->addr != segbase)
	{
		c_segment = c_segment->next;
		if( c_segment == NULL)
		{
			printf("error segment could not be found\n");
			exit(EXIT_FAILURE);
		}
	}
	c_range->segment = c_segment;

	/*create the undo log*/
	c_range->undo = (char*)malloc(size*sizeof(char));
	memcpy(c_range->undo,ptr,size);
	c_range->size = size;
	c_range->addr = ptr;
	c_range->next = c_trans->head;
	c_range->offset = offset;
	c_trans->head = c_range;
}

/*commit all changes into a log file so that if the system crash, the modifications can be applied-----------------------------------------------*/
void rvm_commit_trans(trans_t tid)
{
	int i,abs_addr,header,size, log_fd, seg_fd,res;
	unsigned int offset;
	char log_byte,buf,mod;
	char * filename;
	range_t c_range;
	range_t p_range ;
	seglist_t c_segment;
	trans_tx c_trans = transactions_head;

	errno =0;

	/*find transaction in database*/
	while(c_trans->serial_number != tid)
	{
		c_trans=c_trans->next;
		if(c_trans == NULL)
		{
			printf("error transaction could not be found\n");
			exit(EXIT_FAILURE);
		}	
	}
	c_range = c_trans->head;
	
	/*commit all the modified ranges of the transaction*/
	while(c_range != NULL)
	{
		/*create/open the log file*/
		filename = (char*)malloc(strlen(c_trans->folder) + strlen(c_range->segment->name)+6);
		strcpy(filename,c_trans->folder);
		strcat(filename,"/");
		strcat(filename,c_range->segment->name);
		strcat(filename,".log");
		log_fd = open(filename, O_RDWR | O_CREAT ,  S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
		if(log_fd <0)
		{
			printf("error while creating/opening the segment log file errno %d\n",errno);
			exit(EXIT_FAILURE);
		}
		lseek(log_fd, 0L, SEEK_END);

		/*compute and append to log the modifications*/
		header = 0xFFFFFFFF;
		write(log_fd,&header,sizeof(int));
		write(log_fd,&c_trans->serial_number,sizeof(int));
		for(i=0;i<c_range->size;i++)
		{
			log_byte = (char)*(c_range->undo + i)^(char)*(c_range->addr + i);
			if(log_byte != 0)
			{
				abs_addr = i +c_range->offset;
				write(log_fd,&abs_addr,sizeof(int));
				write(log_fd,&log_byte,1);
			}
		}

		/*check the size of the log file and truncate it if necessary*/
		lseek(log_fd, 0L, SEEK_SET);
		size = lseek(log_fd, 0L,SEEK_END);
		if(size > MAX_LOG_SIZE)
		{
			/*opening the corresponding segment*/
			size = strlen(filename);
			*(filename+size-2) = 'e';
			*(filename+size-3) = 's';
			seg_fd = open(filename, O_RDWR | O_CREAT ,  S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
			if(seg_fd <0)
			{
				printf("error while opening the segment file %s errno %d\n",filename,errno);
				exit(EXIT_FAILURE);
			}

			/*truncating the log file*/
			size = lseek(log_fd, 0L, SEEK_SET);
			read(log_fd,&offset,sizeof(int));
			if(offset!=0xFFFFFFFF)
			{
				printf("error wrong log file format\n");
				exit(EXIT_FAILURE);
			}

			/*skip the transcation serial number*/ 
			lseek(log_fd,sizeof(int),SEEK_CUR);
			buf = 0;
			res = 1;
			while(res !=0)
			{
				res = read(log_fd,&offset,sizeof(int));
				while(offset == 0xFFFFFFFF)
				{
					res = 0;
					offset = 0;

					/*skip the transcation serial number*/ 
					lseek(log_fd,sizeof(int),SEEK_CUR);
					res = read(log_fd,&offset,sizeof(int));
				}
				res = res + read(log_fd,&buf,1);
				if(res == 5)
				{
					/*write the modifications to the segment*/
					lseek(seg_fd,offset,SEEK_SET);
					read(seg_fd,&mod,1);
					lseek(seg_fd,offset,SEEK_SET);
					mod = buf^mod;
					write(seg_fd,&mod,1);
				}
			}
			close(log_fd);
			close(seg_fd);
			size = strlen(filename);
			*(filename+size-2) = 'o';
			*(filename+size-3) = 'l';
			if( (remove(filename) == -1))
			{
				printf("error cannot remove segment log file %s %d\n",filename,errno);
				exit(EXIT_FAILURE);
			}
		}
		else
			close(log_fd);

		free(filename);
		p_range = c_range;
		c_range = p_range->next;
		free(p_range->undo);
		free(p_range);
	}

	/*turn off busy flag of unused segments*/
	for(i = 0;i < c_trans->numsegs;i++)
	{
		c_segment = segments_head;
		while(c_segment != *(c_trans->segbases+i))
		{
			c_segment = c_segment->next;
			if( c_segment == NULL)
			{
				printf("error segment has been unmapped before transaction end\n");
				exit(EXIT_FAILURE);
			}
		}
		c_segment->busy = FALSE;
	}
	free(c_trans);
}

/*abort the transaction, undo all changes occured during the transaction-------------------------------------------------------------------------*/
void rvm_abort_trans(trans_t tid)
{
	range_t c_range;
	range_t n_range ;
	trans_tx c_trans = transactions_head;

	/*find transaction in database*/
	while(c_trans->serial_number != tid)
	{
		c_trans=c_trans->next;
		if(c_trans == NULL)
		{
			printf("error transaction could not be found\n");
			exit(EXIT_FAILURE);
		}	
	}
	c_range = c_trans->head;

	/*restore all the modified ranges of the transaction*/
	while(c_range != NULL)
	{
		memcpy(c_range->addr,c_range->undo,c_range->size);
		n_range = c_range->next;

		/*free undo and ranges*/
		free(c_range->undo);
		free(c_range);
		c_range = n_range;
	}
	/*free the transaction structure*/
	free(c_trans);
}

/*shrinks the log by applying the modifications--------------------------------------------------------------------------------------------------*/
void rvm_truncate_log(rvm_t rvm)
{
	int log_fd,seg_fd,size,res;
	unsigned int offset;
	char * seg_name;
	char buf,mod;
	DIR* dd;
	struct dirent* file;

	errno = 0;

	/* Open the directory */
	if (NULL == (dd = opendir (rvm))) 
	{
		printf("error when opening the backing folder\n");
		exit(EXIT_FAILURE);
	}

	/*scan for log files in directory*/
	while ((file = readdir(dd))) 
    	{
		if(strstr(file->d_name,".log"))
		{
			size = strlen(file->d_name);
			seg_name = (char*)malloc(size+strlen(rvm)+1);
			strcpy(seg_name,rvm);
			strcat(seg_name,"/");
			strcat(seg_name,file->d_name);

			/*open the log file*/
			log_fd =  open(seg_name, O_RDWR | O_CREAT ,  S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
			if(log_fd <0)
			{
				printf("error while opening the segment log file %s errno %d\n",seg_name,errno);
				exit(EXIT_FAILURE);
			}
			
			/*opening the corresponding segment*/
			*(seg_name+size+strlen(rvm)-1) = 'e';
			*(seg_name+size+strlen(rvm)-2) = 's';
			seg_fd = open(seg_name, O_RDWR | O_CREAT ,  S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
			if(seg_fd <0)
			{
				printf("error while opening the segment file %s errno %d\n",seg_name,errno);
				exit(EXIT_FAILURE);
			}

			/*truncating the log file*/
			size = lseek(log_fd, 0L, SEEK_SET);
			read(log_fd,&offset,sizeof(int));
			if(offset!=0xFFFFFFFF)
			{
				printf("error wrong log file format\n");
				exit(EXIT_FAILURE);
			}

			/*skip the transaction serial number*/ 
			lseek(log_fd,sizeof(int),SEEK_CUR);
			buf = 0;
			res = 1;
			while(res !=0)
			{
				res = read(log_fd,&offset,sizeof(int));
				while(offset == 0xFFFFFFFF)
				{
					res = 0;
					offset = 0;

					/*skip the transcation serial number*/ 
					lseek(log_fd,sizeof(int),SEEK_CUR);
					res = read(log_fd,&offset,sizeof(int));
				}
				res = res + read(log_fd,&buf,1);
				if(res == 5)
				{
					/*write the modifications to the segment*/
					lseek(seg_fd,offset,SEEK_SET);
					read(seg_fd,&mod,1);
					lseek(seg_fd,offset,SEEK_SET);
					mod = buf^mod;
					write(seg_fd,&mod,1);
				}
			}
			close(log_fd);
			close(seg_fd);
			size = strlen(file->d_name);
			*(seg_name+size+strlen(rvm)-1) = 'o';
			*(seg_name+size+strlen(rvm)-2) = 'l';
			if( (remove(seg_name) == -1))
			{
				printf("error cannot remove segment log file %s %d\n",seg_name,errno);
				exit(EXIT_FAILURE);
			}
			free(seg_name);
		}
	}
}






